package main

import (
	"fmt"
	"html/template"
	"net/http"

	"golang.org/x/oauth2"
)

func login(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var username = r.FormValue("username")
		var password = r.FormValue("password")

		if username == "huda" && password == "huda" {
			url := "https://localhost:9001/oauth2/auth/requests/consent"
			http.Redirect(w, r, url, http.StatusSeeOther)
		} else {
			fmt.Println("salah")
		}
	}
	var data = map[string]string{
		"title": "login",
	}
	var t, err = template.ParseFiles("template/login.html")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	t.Execute(w, data)
}

func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "ini halaman index")
}

func request(w http.ResponseWriter, r *http.Request) {
	conf := &oauth2.Config{
		ClientID:     "huntr",
		ClientSecret: "some-secret",
		Scopes:       []string{"openid", "offline"},
		Endpoint: oauth2.Endpoint{
			AuthURL:  "https://localhost:9000/oauth2/auth",
			TokenURL: "https://localhost:9000/oauth2/token",
		},
	}

	// Redirect user to consent page to ask for permission
	// for the scopes specified above.
	url := conf.AuthCodeURL("hudaalfariziit9872kahdflkajh", oauth2.AccessTypeOffline)
	http.Redirect(w, r, url, http.StatusSeeOther)
}
func callback(w http.ResponseWriter, r *http.Request) {
	var data = map[string]string{
		"title": "callback",
	}
	var t, err = template.ParseFiles("template/consent.html")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	t.Execute(w, data)
}
func main() {
	http.HandleFunc("/", index)
	http.HandleFunc("/request", request)
	http.HandleFunc("/login", login)
	http.HandleFunc("/callback", callback)
	fmt.Println("starting web server at http://localhost:8080/")
	http.ListenAndServe(":8080", nil)
}
